<?php

namespace Drupal\paragraphs_sets_alter\Event;

/**
 * Class ExportEntityEvents
 *
 * Provides event/hook names for utilization in various places.
 *
 * @package Drupal\export_entity\Event
 */
final class ParagraphsSetsAlterEvents {

  const USE_PARAGRAPHS_SET = 'paragraphs_sets_alter.use_paragraphs_set';

}
