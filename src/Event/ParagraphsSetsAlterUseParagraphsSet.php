<?php

namespace Drupal\paragraphs_sets_alter\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\paragraphs_sets\Entity\ParagraphsSet;

class ParagraphsSetsAlterUseParagraphsSet extends Event {

  /**
   * The paragraph set intended to be used.
   *
   * @var \Drupal\paragraphs_sets\Entity\ParagraphsSet
   */
  protected $set;

  /**
   * The paragraph set context.
   *
   * @var array
   */
  protected $context;

  protected $element;

  protected $usable = TRUE;

  /**
   * The constructor.
   *
   * @param \Drupal\paragraphs_sets\Entity\ParagraphsSet
   *   The paragraph set intended to be used.
   */
  public function __construct($element, $context) {
    $this->element = $element;
    $this->set = paragraphs_set_load($element['#set_machine_name']);
    $this->context = $context;
  }

  public function getSet() {
    return $this->set;
  }

  public function getElement() {
    return $this->element;
  }

  public function setElement($element) {
    $this->element = $element;
  }

  public function getContext() {
    return $this->context;
  }

  public function isUsable() {
    return $this->usable;
  }

  public function setUsable($usable) {
    $this->usable = $usable;
  }

}
